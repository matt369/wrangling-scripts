Wrangling Scripts
=================
Wrangling and logging shell scripts for Avid and EditShare Flow.

Installation
------------
To install the scripts, run:

    make install

Scripts
-------
### aatotl
Convert Avid ALE files to tape log TSV files.

Usage:

    aatotl [-fnv] [-d dest_dir] file ...

Options:

    -d dest_dir
          Specify the destination directory.
    -f    Overwrite existing files without prompting.
    -n    Do not include a column header line.
    -v    Print version information to standard output, then exit.

### cppath
Copy the path for each file within a directory.

Usage:

    cppath [-Aauv] [directory ...]

Options:

    -A    Only ignore the implied . and .. directories.
    -a    Include all files whose names begin with a dot (.).
    -u    Do not convert Unix paths to Windows paths.
    -v    Print version information to standard output, then exit.

Dependencies:

    clip.exe | pbcopy | xclip | xsel

### mkh264
Transcode videos to H.264 using ffmpeg.

Usage:

    mkh264 [-v] [-c crf_value] file ...

Options:

    -c crf_value
          Define the constant rate factor value (default 16).
    -v    Print version information to standard output, then exit.

Dependencies:

    ffmpeg

### tltoaa
Convert tape log TSV files to Avid ALE files.

Usage:

    tltoaa [-fv] [-d dest_dir] file ...

Options:

    -d dest_dir
          Specify the destination directory.
    -f    Overwrite existing files without prompting.
    -v    Print version information to standard output, then exit.

### tltofx
Convert tape log TSV files to Flow XML files.

Usage:

    tltofx [-fv] [-d dest_dir] file ...

Options:

    -d dest_dir
          Specify the destination directory.
    -f    Overwrite existing files without prompting.
    -v    Print version information to standard output, then exit.

Tape Log TSV File
-----------------
A plain (UTF-8) text file containing the following tab-separated values:

1. Clipname
2. Scene
3. Take
4. Comments
5. Project
6. Tape (optional)

When a tape log TSV file is parsed any lines beginning with a hash character (#) are ignored.
