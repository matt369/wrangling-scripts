NAME = wrangling-scripts
VERSION = 2020.03.27
SCRIPTS = aatotl cppath mkh264 tltoaa tltofx
PREFIX ?= /usr/local

all: dist

clean:
	@rm -f ${NAME}-${VERSION}.tar.gz
	@printf 'cleaned ${NAME}\n'

dist: clean
	@mkdir -p ${NAME}-${VERSION}
	@cp -R LICENSE Makefile README.md ${SCRIPTS} ${NAME}-${VERSION}
	@tar -cf ${NAME}-${VERSION}.tar ${NAME}-${VERSION}
	@gzip ${NAME}-${VERSION}.tar
	@rm -Rf ${NAME}-${VERSION}
	@printf 'created distribution ${NAME}-${VERSION}.tar.gz\n'

install: ${SCRIPTS}
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@for file in $?; do \
		sed 's/PKG/${NAME}/g;s/VERSION/${VERSION}/g' "$$file" >"${DESTDIR}${PREFIX}/bin/$$file" && \
		chmod 0755 "${DESTDIR}${PREFIX}/bin/$$file" && \
		printf 'installed %s to ${DESTDIR}${PREFIX}/bin\n' "$$file"; \
		done

uninstall:
	@for file in ${SCRIPTS}; do \
		[ -f "${DESTDIR}${PREFIX}/bin/$$file" ] || continue; \
		rm -f "${DESTDIR}${PREFIX}/bin/$$file" && \
		printf 'uninstalled ${DESTDIR}${PREFIX}/bin/%s\n' "$$file"; \
		done

.PHONY: all clean dist install uninstall
